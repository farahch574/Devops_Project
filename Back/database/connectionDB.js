const mongoose = require("mongoose");

const connectDB = () => {
  mongoose.set("strictQuery", false);
  mongoose
    .connect(process.env.DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(() => {
      console.log(`Connected tfffmmmo MongoDB ddffeefdd  db...`);
    })
    .catch((error) => {
      console.error(`Error connvvecting to MongoDB: ${error}`);
    });
};
module.exports = connectDB;
