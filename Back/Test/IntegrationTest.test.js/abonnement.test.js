const request = require("supertest");
const express = require("express");
const mongoose = require("mongoose");
const AbonnementModel = require("../../models/AbonnementModel");

const app = express();
const server = require("http").createServer(app);
const mongoUri = "mongodb://127.0.0.1:27017/tv";

beforeAll((done) => {
  server.listen(8080, async () => {
    console.log(`Listening on port 8080...`);
    await mongoose.connect(mongoUri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    done();
  });
}, 10000);

/* afterAll(async (done) => {
  await mongoose.disconnect();
  server.close(done); // Close the server and call done when done
}, 10000); */

describe("Abonnement Controller - Integration Tests", () => {
  test("should add an abonnement and return success", async () => {
    const abonnementData = {
      application: "Sed ipsum maxime lor",
      adresseMac: "XX:XX:XX:XX:XX:XX",
      dateDebut: "2007-10-30",
      dateFin: new Date("Fri May 30 2008 01:00:00 GMT+0100 (UTC+01:00)"),
      periode: 7,
      clientID: "6586d2b75752fb4ce5ef4e6f",
      deviceID: "6586d3115752fb4ce5ef4e82",
      typeAbonnID: "6586d3215752fb4ce5ef4e94",
      etatPaiement: "NOT_PAIED",
    };

    const response = await request(app).post(
      "http://localhost:8080/abonnement/add"
    );

    expect(response.status).toBe(200);
    expect(response.body.success).toBe(true);
    expect(response.body.data).toBeDefined();

    await AbonnementModel.deleteOne({ _id: response.body.data._id });
  });

  // The other test is commented out for now since it references an endpoint that is not defined
});
