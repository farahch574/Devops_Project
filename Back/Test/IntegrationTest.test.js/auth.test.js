const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const {
  Login,
  GetUserByToken,
  RefreshToken,
  ChangePassword,
  ForgotPassword,
} = require("../../auth/AuthentificationController"); // Adjust the path as needed

const UserModel = require("../../models/UserModel"); // Adjust the path as needed
const GenerateToken = require("../../functions/GenerateToken"); // Adjust the path as needed

jest.mock("bcrypt");
jest.mock("jsonwebtoken");
jest.mock("../../functions/GenerateToken"); // Mock the GenerateToken module

describe("Authentication Controller Tests", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should log in user successfully", async () => {
    const req = {
      body: {
        email: "user@example.com",
        password: "password123",
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    const mockUser = {
      _id: "mockUserId",
      email: req.body.email,
      password: "hashedPassword",
    };

    // Mocking the UserModel.findOne method with a resolved value
    UserModel.findOne = jest.fn().mockResolvedValue(mockUser);
    bcrypt.compare.mockResolvedValue(true);
    GenerateToken.AccessToken.mockReturnValue("mockAccessToken");

    await Login(req, res);

    expect(UserModel.findOne).toHaveBeenCalledWith({ email: req.body.email });
    expect(bcrypt.compare).toHaveBeenCalledWith(
      req.body.password,
      mockUser.password
    );
    expect(GenerateToken.AccessToken).toHaveBeenCalledWith(
      { _id: mockUser._id },
      "3000s"
    );
    // Add more assertions based on the implementation of Login function

    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalled();

    const responseCalls = res.json.mock.calls; // Retrieve all calls to res.json

    // Inspect each call to res.json and log the arguments passed
    responseCalls.forEach((args, index) => {
      console.log(`Call ${index + 1} to res.json:`, args);
    });
  });

  // Add more test cases for different scenarios in Login, RefreshToken, ChangePassword, ForgotPassword

  // Remember to test error scenarios, edge cases, and success paths in each function.
});
