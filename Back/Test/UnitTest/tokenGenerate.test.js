// tokenGenerator.test.js

const jwt = require("jsonwebtoken");
const { AccessToken, RefreshToken } = require("../../functions/GenerateToken");

jest.mock("jsonwebtoken");

describe("Token Generator", () => {
  const mockData = { userId: "123", username: "testuser" };
  const mockAccessTokenSecret = "mockAccessTokenSecret";
  const mockRefreshTokenSecret = "mockRefreshTokenSecret";

  beforeAll(() => {
    process.env.ACCESS_TOKEN_SECRET = mockAccessTokenSecret;
    process.env.REFRESH_TOKEN_SECRET = mockRefreshTokenSecret;
  });

  afterAll(() => {
    delete process.env.ACCESS_TOKEN_SECRET;
    delete process.env.REFRESH_TOKEN_SECRET;
  });

  test("generates an access token", () => {
    jwt.sign.mockReturnValue("mockAccessToken");

    const duration = "1h";
    const result = AccessToken(mockData, duration);

    expect(jwt.sign).toHaveBeenCalledWith(mockData, mockAccessTokenSecret, {
      expiresIn: duration,
    });
    expect(result).toBe("mockAccessToken");
  });

  test("generates a refresh token", () => {
    jwt.sign.mockReturnValue("mockRefreshToken");

    const duration = "7d";
    const result = RefreshToken(mockData, duration);

    expect(jwt.sign).toHaveBeenCalledWith(mockData, mockRefreshTokenSecret, {
      expiresIn: duration,
    });
    expect(result).toBe("mockRefreshToken");
  });
});
