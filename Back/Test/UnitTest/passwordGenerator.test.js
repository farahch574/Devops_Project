const GeneratePassword = require("../../functions/GeneratePassword");
describe("randomPasswordGenerator", () => {
  test("generates a string of the correct length", () => {
    const result = GeneratePassword();
    expect(result.length).toBe(12);
  });

  test("generates a string containing only valid characters", () => {
    const result = GeneratePassword();
    const validChars =
      "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz*/-+";
    expect([...result].every((char) => validChars.includes(char))).toBe(true);
  });

  test("generates different strings on different calls", () => {
    const result1 = GeneratePassword();
    const result2 = GeneratePassword();
    expect(result1).not.toBe(result2);
  });
  test("generates strings of the correct length", () => {
    const length = 12;
    const result = GeneratePassword(length);
    expect(result.length).toBe(length);
  });
});
